<?php
class Database{
  private $pdo;

  public function __construct($schema){
    if (!$settings = parse_ini_file("/server/config/mysql-database.ini", TRUE));
      $this->pdo = new PDO($settings['database']['driver'] .
        ':host=' . $settings['database']['host'] .
        ((!empty($settings['database']['port'])) ? (';port=' . $settings['database']['port']) : '') .
        ';dbname=' . $schema, $settings['database']['username'], $settings['database']['password']);
  }
  public function prepared_statement($query, $array_values){
    $st = $this->pdo->prepare($query);
    $st->execute($array_values);
    return $st;
  }

  public function user_id_exists($id){
    $st = $this->prepared_statement("SELECT id FROM users WHERE id = ?", array($id));
    return $st->rowCount()>0;
  }
  public function xss($data){
      foreach ($data as $var) {
        $var = htmlspecialchars($var);
      }
      return $data;
  }
  public function encrypt_text($text, $pass){
    return openssl_encrypt($text, "AES-256-CBC", $pass);
  }
  public function decrypt_text($crypt, $pass){
    return openssl_decrypt($crypt, "AES-256-CBC", $pass);
  }
}

 ?>
