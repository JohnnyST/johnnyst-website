<?php
require_once($_SERVER['DOCUMENT_ROOT']."/data/config/database.php");
class CodenamesDatabase extends Database{

    public function __construct(){
      parent::__construct("data");
    }

  public function create_game($sizeX, $sizeY, $code, $post){
    $arr = array('o');
    for ($i=1; $i < $sizeX*$sizeY; $i++) {
      if($i<=$sizeX*$sizeY/2){
        $arr[$i] = 'r';
      }
      else{
        $arr[$i] = 'b';
      }
    }
    shuffle($arr);
    $this->prepared_statement('INSERT INTO games (id, sizeX, sizeY) VALUES (?, ?, ?);', array($code, $sizeX, $sizeY));
    $i = 0;
    for ($x=0; $x < $sizeX; $x++) {
        for ($y=0; $y < $sizeY; $y++) {
          $this->prepared_statement('INSERT INTO cells (game_code, x, y, word, team) VALUES (?, ?, ?, ?, ?);', array($code, $x, $y, $post['cell-'.$x."-".$y], $arr[$i]));
          $i++;
        }
    }
  }

public function add_player($code, $id, $name){
  $n = $this->prepared_statement("SELECT COUNT(id) FROM players WHERE game_code = ?", array($code))->fetch()[0];
  if($n % 2 == 0)$t = 'r';
  else $t = 'b';
  $st = $this->prepared_statement("SELECT id FROM players WHERE id = ?", array($id));
  if($st->rowCount()>0){
    $this->prepared_statement("UPDATE players SET game_code=?,name=?,explainer=0,team=? WHERE id = ?", array($code,$name,$t,$id));
  }
  else{
    $this->prepared_statement("INSERT INTO players (id,game_code,name,team) VALUES (?,?,?,?)", array($id,$code,$name,$t));
  }
}
public function get_team($id){
  return $this->prepared_statement("SELECT team FROM players WHERE id = ?", array($id))->fetch()[0];
}
public function team_has_explainer($code, $team){
  return $this->prepared_statement("SELECT COUNT(id) FROM players WHERE game_code = ? AND team = ? AND explainer = 1", array($code, $team))->fetch()[0]>0;
}
public function player_team_has_explainer($code, $id){
  return $this->prepared_statement("SELECT COUNT(id) FROM players WHERE game_code = ? AND team = (SELECT team FROM players WHERE id = ?) AND explainer = 1", array($code, $id))->fetch()[0]>0;
}
public function is_explainer($id){
  return $this->prepared_statement("SELECT explainer FROM players WHERE id = ?", array($id))->fetch()[0];
}
public function claim_explainer($code, $id){
  $this->prepared_statement("UPDATE players SET explainer=1 WHERE id = ?", array($id));
}
    public function game_exists($code){
      $st = $this->prepared_statement("SELECT id FROM games WHERE id = ?", array($code));
      return $st->rowCount()>0;
    }
    public function get_winner($code){
      return $this->prepared_statement("SELECT winner FROM games WHERE id = ?", array($code))->fetch()[0];
    }
    public function set_winner($code, $t){
      $this->prepared_statement("UPDATE games SET winner = ? WHERE id = ?", array($t, $code))->fetch()[0];
    }
public function select_cell($code, $x, $y){
$this->prepared_statement("UPDATE cells SET selected = 1 WHERE game_code = ? AND x = ? AND y = ?", array($code, $x, $y));
}
public function reveal_cell($code, $x, $y, $id){
$this->prepared_statement("UPDATE cells SET revealed = 1, selected = 0 WHERE game_code = ? AND x = ? AND y = ?", array($code, $x, $y));
$t = $this->get_team($id);
  if($this->prepared_statement("SELECT COUNT(id) FROM cells WHERE game_code = ? AND team = ? AND revealed = 0", array($code, $t))->fetch()[0]==0){
    $this->set_winner($code, $t);
  }

}
public function get_players($code, $t){
  return $this->prepared_statement("SELECT name, explainer FROM players WHERE game_code = ? AND team = ?", array($code, $t));
}
public function bomb_hit($code, $id){
  $t = $this->get_team($id);
  if($t=="r")$this->set_winner($code, "b");else $this->set_winner($code, "r");
}
public function is_bomb_hit($code){
  return $this->prepared_statement("SELECT revealed FROM cells WHERE game_code = ? AND team = 'o'", array($code))->fetch()==1;
}
public function cell_team($code, $x, $y){
return $this->prepared_statement("SELECT team FROM cells WHERE game_code = ? AND x = ? AND y = ?", array($code, $x, $y))->fetch()[0];
}
public function selected_cells_count($code){
return $this->prepared_statement("SELECT COUNT(id) FROM cells WHERE selected = 1 AND game_code = ?", array($code))->fetch()[0];
}
public function next_turn($code){
  $turn = $this->current_turn($code);
  if($turn == "r")$turn = "b";
  else $turn = "r";
  $this->prepared_statement("UPDATE games SET turn = ? WHERE id = ?", array($turn, $code));
  $this->prepared_statement("UPDATE cells SET selected = 0 WHERE game_code = ?", array($code));
}
  public function current_turn($code){
    $st = $this->prepared_statement("SELECT turn FROM games WHERE id = ?", array($code));
    return $st->fetch()['turn'];
  }

  public function get_cells($code){
    return $this->prepared_statement("SELECT word, revealed, team, selected FROM cells WHERE game_code = ? ORDER BY y, x", array($code));
  }

  public function get_size($code){
    $st = $this->prepared_statement("SELECT sizeX, sizeY FROM games WHERE id = ?", array($code));
    return $st->fetch();

  }
}
?>
