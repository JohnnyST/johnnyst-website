<?php
require_once($_SERVER['DOCUMENT_ROOT']."/data/config/database.php");
class DreamDatabase extends Database{
  private $table = 'dreams';

  public function __construct(){
    parent::__construct("data");
  }
  public function insert_new(){
    $n = $this->users_max()+1;
    $this->prepared_statement("INSERT INTO $this->table ".'(user_id, users_number, public, title, text, color, sleep_start, sleep_end, lucid, favourite, nightmare) VALUES (?, ?, 0, "", "", "'."#".'d37b17", '.mktime().', '.mktime().', 0, 0, 0)', array($_SESSION['login'], $n));
    return $n;
  }
  public function get_dream_by_number($dream_number){
    return $this->prepared_statement("SELECT users_number, title, text, color, sleep_start, sleep_end, lucid, nightmare, favourite, public FROM $this->table WHERE user_id = ? AND users_number = ?", array($_SESSION['login'], $dream_number))->fetch(PDO::FETCH_ASSOC);
  }
  public function get_dreams($page, $page_length){
    $l = $page * $page_length;
    return $this->prepared_statement("SELECT users_number, title, text, color, sleep_start, sleep_end, lucid, nightmare, favourite, public FROM $this->table WHERE user_id = ? ORDER BY sleep_end DESC LIMIT $l, $page_length", array($_SESSION['login']));
  }
  public function get_all_dreams($page, $page_length){
    $l = $page * $page_length;
    return $this->prepared_statement("SELECT username, users_number, title, text, color, sleep_start, sleep_end, lucid, nightmare, favourite, public FROM $this->table JOIN users ON $this->table.user_id=users.id WHERE public = 1 ORDER BY sleep_end DESC LIMIT $l, $page_length", array($_SESSION['login']));
  }
  // Return highest dream number from the user.
  public function users_max(){
    return $this->prepared_statement("SELECT MAX(users_number) FROM $this->table WHERE user_id = ?", array($_SESSION['login']))->fetch()['MAX(users_number)'];
  }
  public function edit($users_number, $data){
    $code = $this->validate_dream_input($data);
    if($code!=0)echo $code;
    $dream_data = $this->dream_data_array($data);
    $dream_data['users_number'] = $users_number;
    $this->prepared_statement("UPDATE $this->table SET public=:public, title=:title, text=:text, color=:color, sleep_start=:sleep_start, sleep_end=:sleep_end, lucid=:lucid, favourite=:favourite, nightmare=:nightmare WHERE user_id=:user_id AND users_number=:users_number", $dream_data);
    echo $code;
  }

  private function validate_dream_input($data){
    if(!isset($_SESSION['login'])){
      return 1;
    }
    if(!$this->user_id_exists($_SESSION['login'])){
      return 2;
    }
    if(isset($data['public'])&&!isset($_SESSION['password'])){
      return 3;
    }
    if(strlen($data['title'])>64){
      return 10;
    }
    if(strlen($data['color'])>7||!preg_match('/(#[a-f0-9]{3}([a-f0-9]{3})?)/i', $data['color'])){
      return 11;
    }
    return 0;
  }
  private function dream_data_array($data){
    $data["sleep_start"] = strtotime($data["date"]." ".$data['sleep_start']);
    $data["sleep_end"] = strtotime($data["date"]." ".$data['sleep_end']);
    if($data["sleep_start"]>$data["sleep_end"]){
      $data["sleep_start"] -= 86400;
    }
    if(!isset($data['public']) || $data['public']==0){
      $data['text'] = $this->encrypt_text($data['text'], $_SESSION['password']);
    }
    $data['user_id'] = $_SESSION['login'];
    if(!isset($data['title'])) $data['title'] = "";
    if(!isset($data['text'])) $data['text'] = "";
    if(!isset($data['color'])) $data['color'] = "#d37b17";
    if(!isset($data['sleep_start'])) $data['sleep_start'] = 0;
    if(!isset($data['sleep_end'])) $data['sleep_end'] = 0;
    if(isset($data['public']) && $data['public'] == 1) $data['public'] = 1;else $data['public'] = 0;
    if(isset($data['lucid']) && $data['lucid'] == 1) $data['lucid'] = 1;else $data['lucid'] = 0;
    if(isset($data['favourite']) && $data['favourite'] == 1) $data['favourite'] = 1;else $data['favourite'] = 0;
    if(isset($data['nightmare']) && $data['nightmare'] == 1) $data['nightmare'] = 1;else $data['nightmare'] = 0;
    unset($data['date']);
    return $data;
  }
  public function delete_dream($users_number){
    $this->prepared_statement("DELETE FROM $this->table WHERE user_id = ? AND users_number = ?", array($_SESSION['login'], $users_number));
  }
  public function statistics(){
    $stats = array();
    $res = $this->prepared_statement("SELECT color AS color, COUNT(*) AS count FROM $this->table WHERE user_id = ? GROUP BY color ORDER BY color", array($_SESSION['login']))->fetchAll(PDO::FETCH_ASSOC);
    foreach ($res as $key => $value) {
      $stats['color-dist'][$value['color']] = $value['count'];
    }
    $stats['count'] = $this->prepared_statement("SELECT COUNT(*) AS count FROM $this->table WHERE lucid = 0 AND user_id = ?", array($_SESSION['login']))->fetch()['count'];
    $stats['count-lucid'] = $this->prepared_statement("SELECT COUNT(*) AS count FROM $this->table WHERE lucid = 1 AND user_id = ?", array($_SESSION['login']))->fetch()['count'];

    $stats['public-count'] = $this->prepared_statement("SELECT COUNT(*) AS count FROM $this->table WHERE lucid = 0 AND public = 1", array())->fetch()['count'];
    $stats['public-count-lucid'] = $this->prepared_statement("SELECT COUNT(*) AS count FROM $this->table WHERE lucid = 1 AND public = 1", array())->fetch()['count'];
    $stats['avg-sleep-time'] = $this->prepared_statement("SELECT AVG(sleep_end-sleep_start) AS avg FROM $this->table WHERE user_id = ? AND sleep_end > ?", array($_SESSION['login'], time()-2592000))->fetch()['avg'];
    $res = $this->prepared_statement("SELECT sleep_end, sleep_start FROM $this->table WHERE user_id = ? ORDER BY sleep_end DESC LIMIT 100", array($_SESSION['login']))->fetchAll(PDO::FETCH_ASSOC);
    foreach ($res as $key => $value) {
      $stats['sleep_length'][$value['sleep_end']] = $value['sleep_end']-$value['sleep_start'];
    }
    $res = $this->prepared_statement("SELECT sleep_end, LENGTH(text) AS text_length FROM $this->table WHERE user_id = ? ORDER BY sleep_end DESC", array($_SESSION['login']))->fetchAll(PDO::FETCH_ASSOC);
    foreach ($res as $key => $value) {
      $stats['text_length'][$value['sleep_end']] = $value['text_length'];
    }
    return json_encode($stats);
  }

  public function get_public_dream_count(){
    return $this->prepared_statement("SELECT COUNT(*) AS count FROM $this->table WHERE public = 1", array())->fetch()['count'];
  }
  public function get_dream_count(){
    return $this->prepared_statement("SELECT COUNT(*) AS count FROM $this->table WHERE user_id = ?", array($_SESSION['login']))->fetch()['count'];
  }

  public function get_colors(){
    return $this->prepared_statement("SELECT DISTINCT color FROM $this->table WHERE user_id = ?", array($_SESSION['login']));
  }
  public function get_owner_id($dream_id){
    return $this->prepared_statement("SELECT user_id FROM $this->table WHERE id = ?", array($dream_id))->fetch()[0];
  }
  public function get_dream($dream_id){
    return $this->prepared_statement("SELECT * FROM $this->table WHERE id = ?", array($dream_id))->fetch();
  }
}
function formatMilliseconds($seconds) {
    $minutes = floor($seconds / 60);
    $hours = floor($minutes / 60);
    $seconds = $seconds % 60;
    $minutes = $minutes % 60;

    $format = '%d hours, %d minutes';
    return sprintf($format, $hours, $minutes);
}
