<?php
require_once($_SERVER['DOCUMENT_ROOT']."/data/config/database.php");
class LoginDatabase extends Database{
  private $table = 'users';

  public function __construct(){
    parent::__construct("data");
  }
  public function user_exists($name){
    $st = $this->prepared_statement("SELECT id FROM users WHERE username = ?", array($name));
    return $st->rowCount()>0;
  }
  public function select_user($name){
    $st = $this->prepared_statement("SELECT id, username, password_hash, email FROM $this->table WHERE username = ?", array($name));
    if($st->rowCount()>0){
      return $st->fetch();
    }
    else {
      return null;
    }
  }
  public function insert_user($name, $password_hash){
    $result = $this->prepared_statement("INSERT INTO $this->table (username, password_hash) VALUES (?, ?);", array($name, $password_hash));
    return $result;
  }
  public function get_user($id){
    return $this->prepared_statement("SELECT * FROM $this->table WHERE id = $id;", array($id))->fetch();
  }
  public function get_username($id){
    return $this->prepared_statement("SELECT username FROM $this->table WHERE id = $id;", array($id))->fetch()[0];
  }
  public function validate_signup($data){
    if(!isset($data['name']) || !strlen(htmlspecialchars($data['name'])) > 0){
      return 1; // name not entered
    }
    else if(!isset($data['password']) || !strlen(htmlspecialchars($data['password'])) > 0){
      return 2; // password not entered
    }
    else if(!isset($data['password_repeat']) || !strlen(htmlspecialchars($data['password_repeat'])) > 0){
      return 3; // repeated password not entered
    }
    else{
      $name = htmlspecialchars($data['name']);
      $password = htmlspecialchars($data['password']);
      $password_repeat = htmlspecialchars($data['password_repeat']);
      if($password != $password_repeat){
        return 10; // repeated password does not match
      }
      else if(strlen($password) > 72){
        return 11; // Password too long
      }
      else if(strlen($name) > 128){
        return 12; // Name too long
      }
      else if($this->user_exists($name)){
        return 20; // Name already exists
      }
      else{
        $result = $this->insert_user($name, password_hash($password, PASSWORD_BCRYPT));
        if($this->user_exists($name)){
          return 0; // success
        }
        else{
          return 21; // unexpected error
        }
      }
    }
  }
  public function validate_login($data){
    if(!isset($data['name'])||$data['name']==''){
      return 1;
    }
    else if(!isset($data['password'])||$data['password']==''){
      return 2;
    }
    $username = htmlspecialchars($data['name']);
    $password = htmlspecialchars($data['password']);
    $user = $this->select_user($username);
    if(!$this->user_exists($username)){
      return 3;
    }
    else{
      $sql_name = $user['username'];
      $sql_password = $user['password_hash'];
      if(password_verify($password, $sql_password) && $sql_name == $username){
        $_SESSION['login'] = $user['id'];
        $_SESSION['password'] = $password;
        return 0; // login successful
      }
      else {
        return 4; // password wrong
      }
    }
  }
}
 ?>
