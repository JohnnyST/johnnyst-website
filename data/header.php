<?php session_start();
require_once($_SERVER['DOCUMENT_ROOT']."/data/config/login_database.php");
  $db = new LoginDatabase();if(isset($_POST['logout'])){
    unset($_SESSION['login']);
    unset($_SESSION['password']);
  } ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" href="/css/master.css">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5.0, minimum-scale=1">
  </head>
  <body>
    <header id="header">
    <div id="subnav">
      <?php if(isset($_SESSION['login'])){
        echo '<span>Signed in as <i>'.($db->get_username($_SESSION['login'])).'</i>.</span><form action="." method="post"><input class="logout-button" value="Log Out" type="submit" name="logout"></form>';
      } ?>
      <a href="/notice">Site Notice</a>
      <a href="/privacypolicy">Privacy Policy</a>
    </div>
      <a id="title" href="/">
        <img src="/pictures/logo-glasses.png" alt="Logo with sunglasses and headband">
        <h1>JohnnyST</h1>
      </a>
    </header>
    <nav id="nav">
      <div class="<?php if($nav==1)echo "selected "; ?>"><a href="/">Home</a></div>
      <div class="<?php if($nav==2)echo "selected "; ?>"><a href="/dreams">Dreams</a></div>
      <div class="<?php if($nav==3)echo "selected "; ?> nav-login-link"><a href="/login">Login</a></div>
    </nav>
