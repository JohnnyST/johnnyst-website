<?php
  $title = "JohnnyST - Sign Up";
  $nav = 3;
  require($_SERVER['DOCUMENT_ROOT']."/data/header-page.php");
  $db = new LoginDatabase();
  $code = -1;
  if(isset($_POST['submit'])){
    $code = $db->validate_signup($db->xss($_POST));
  }
?>
<div>
  <?php if($code!=0){ ?>
<form class="" action="." method="post">
<label for="name">Username:</label><input type="text" name="name"><br>
<label for="password">Password:</label><input type="password" name="password"><br>
<label for="password_repeat">Repeat password:</label><input type="password" name="password_repeat"><br>
<input type="submit" name="submit">
</form>
<?php }
  if($code==0){
    echo "<p>Account successfully created.</p>";
  }
  else if($code==1){
    echo "<p>Please enter a username.</p>";
  }
  else if($code==2){
    echo "<p>Please enter a password.</p>";
  }
  else if($code==3){
    echo "<p>Please repeat your password.</p>";
  }
  else if($code==11){
    echo "<p>Your password may not be longer than 72 characters.</p>";
  }
  else if($code==10){
    echo "<p>Your repeated password does not match.</p>";
  }
  else if($code==12){
    echo "<p>Your name may not be longer than 128 characters.</p>";
  }
  else if($code==20){
    echo "<p>That username already exists.</p>";
  }
  else if($code==21){
    echo "<p>An unexpected error occurred.</p>";
  }
 ?>
</div>
<?php
  require($_SERVER['DOCUMENT_ROOT'].'/data/footer-page.php');
?>
