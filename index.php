<?php
$title = "JohnnyST - Home";
$nav = 1;
require($_SERVER['DOCUMENT_ROOT']."/data/header.php"); ?>
<link rel="stylesheet" href="/css/home.css">
<main id="main">
      <article class="home-image right-image" style="background-image: url('/pictures/home_5.jpg');">

          <h2>Rock And Roll</h2>
      </article>
      <aside>
        <a href="https://open.spotify.com/artist/1pgCkAdGX82J8bA5fFTLaP?si=ro4iPyM5SJiIlhbwMTZtaA&dl_branch=1">
          <img src="/pictures/spotify.png">
        <h3>Spotify</h3>
        <p>Johnny on Spotify</p>
      </a>
      </aside>
      <article class="home-image left-image" style="background-image: url('/pictures/home_4.jpg');">
          <h2>Coolness</h2>
      </article>
      <aside>
        <a href="https://discord.gg/3thxdqsSKD" target="_blank">
          <img src="/pictures/discord.png">
        <h3>Johnny &amp; Friends</h3>
        <p>Johnny&apos;s Discord Server</p>
      </a>
      </aside>
      <article class="home-image right-image" style="background-image: url('/pictures/home_3.jpg');">
        <h2>SG</h2>
      </article>
      <aside>
        <a href="https://www.youtube.com/channel/UCW7o_Y2g03N9TLVQxP2nQ3Q" target="_blank">
          <img src="/pictures/youtube.png">
        <h3>JohnnyST</h3>
        <p>Johnny&apos;s YouTube Channel</p>
        </a>
      </aside>
      <article class="home-image left-image" style="background-image: url('/pictures/home_7.jpg');">
          <h2>Blues</h2>
      </article>
      <aside>
        <a href="https://www.reddit.com/u/Johnny_ST" target="_blank">
          <img src="/pictures/reddit.png">
        <h3>u&#47;Johnny_ST</h3>
        <p>Memes and Stuff</p>
        </a>
      </aside>
      <article class="home-image right-image" style="background-image: url('/pictures/home_6.jpg');">
<h2>Harmonica</h2>
      </article>
      <aside>
        <a href="https://www.instagram.com/johnnystrot/" target="_blank">
          <img src="/pictures/instagram.png">
        <h3>&commat;johnnystrot</h3>
        <p>Johnny&apos;s Ego Valve</p>
        </a>
      </aside>
      <article class="home-image left-image" style="background-image: url('/pictures/home_1.jpg');">
        <h2>Scream</h2>
      </article>
      <aside>
        <a>
          <img src="/pictures/snapchat.png">
        <h3>johnnystrot</h3>
        <p>Add Me If You Dare</p>
        </a>
      </aside>
      <article class="home-image right-image" style="background-image: url('/pictures/home_2.jpg');">
<h2>Turn It Up</h2>
      </article>
            <aside>
              <a>
                <img src="/pictures/minecraft.png">
              <h3>Minecraft</h3>
              <p>IP: johnnyst.de</p>
              </a>
            </aside>
    </main>
  </body>
</html>
