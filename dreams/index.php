<?php
  $title = "JohnnyST - Dream Diary";
  $nav = 2;
  require($_SERVER['DOCUMENT_ROOT']."/data/header-page.php");
?>
<link rel="stylesheet" href="/css/dreams.css">
<?php if(isset($_SESSION['login'])) {?>
<section class="dreams-headline"><p><h4>Welcome to your dream diary, <?php echo ($db->get_username($_SESSION['login'])) ?>!</h4></p>
  <div class="dreams-navigation">
    <span class="dreams-navigation-button" onclick="toDreamDiary()">Your Dream Diary</span>
    <span class="dreams-navigation-button" onclick="addNewDream()">Add A New Dream</span>
    <span class="dreams-navigation-button" onclick="toPublicDreams()">Public Dreams</span>
    <span class="dreams-navigation-button" onclick="toStatistics()">Statistics</span>
  </div>
</section>
<section class="dreams-main-div">
  <div id="dreams-div-user-list" class="dreams-dream-list">
    <div>
      <h3>Your Dream Diary</h3>
      <div class="dreams-page-selector">
        <p class="dreams-page-button" onclick="previousDreamPage(false);">Previous</p>
        <p class="dreams-page-info">Page <input id="dreams-page-text-input" class="dreams-page-text-input" onchange="dreamPageTextInput()" type="text"></input> of <span id="dreams-page-count"></span></p>
        <p class="dreams-page-button" onclick="nextDreamPage(false);">Next</p>
      </div>
    </div>
    <div id="dreams-user-dream-list" class="dreams-dream-list-dreams">

    </div>
  </div>
  <div id="dreams-div-edit-dream" class="dreams-edit-dream">
    <div class="dreams-edit-title-options">
      <div class="dreams-edit-options">
        <span class="dreams-edit-save-changes" onclick="saveChanges()">Save&nbsp;Changes</span>
        <span class="dreams-edit-save-changes" onclick="deleteDream()">Delete&nbsp;Dream</span>
      </div>
      <div>
        <input id="dreams-edit-title" class="dreams-edit-title" type="text" name="dreams-edit-title" value="" placeholder="Title"></p>
      </div>
      <div>
      </div>
    </div>
    <div class="dreams-edit-flexbox">
      <div class="dreams-edit-data">
        <div>
          <label class="dreams-edit-color" for="dreams-edit-color">Color:</label><br>
          <input id="dreams-edit-color" class="dreams-edit-color" onchange="setColor();" type="color" name="dreams-edit-color" value="#d37b17">
        </div>
        <div class="dreams-edit-date">
          <label class="dreams-edit-date" for="dreams-edit-date">Date of wake up: </label><br>
          <input id="dreams-edit-date" class="dreams-edit-date" value="" type="date" name="dreams-edit-date">
        </div>
        <div>
          <label class="dreams-edit-sleep-start" for="dreams-edit-sleep-start">Sleep Start: </label><br>
          <input id="dreams-edit-sleep-start" class="dreams-edit-sleep-start" value="" type="time" name="dreams-edit-sleep-start">
        </div>
        <div>
          <label class="dreams-edit-sleep-end" for="dreams-edit-sleep-end">Sleep End: </label><br>
          <input id="dreams-edit-sleep-end" class="dreams-edit-sleep-end" value="" type="time" name="dreams-edit-sleep-end">
        </div>
      </div>
      <div class="dreams-edit-text-title">
        <textarea id="dreams-edit-text" class="dreams-edit-text" name="dreams-edit-text" rows="20" cols="40" placeholder="Text"></textarea>
      </div>
      <div class="dreams-edit-checkboxes">
        <div>
          <label class="dreams-edit-lucid" for="dreams-edit-lucid"><img class="dreams-icon" src="lucid.svg">Did you experience<wbr> a lucid dream? </label>
          <input id="dreams-edit-lucid" class="dreams-edit-lucid" type="checkbox" name="dreams-edit-lucid">
        </div>
        <div>
          <label class="dreams-edit-nightmare" for="dreams-edit-nightmare"><img class="dreams-icon" src="nightmare.svg">Did you suffer<wbr> from a nightmare? </label>
          <input id="dreams-edit-nightmare" class="dreams-edit-nightmare" type="checkbox" name="dreams-edit-nightmare">
        </div>
        <div>
          <label class="dreams-edit-favourite" for="dreams-edit-favourite"><img class="dreams-icon" src="favourite.svg">Do you want to mark<wbr> the dream as favourite? </label>
          <input id="dreams-edit-favourite" class="dreams-edit-favourite" type="checkbox" name="dreams-edit-favourite">
        </div>
          <div>
            <label class="dreams-edit-public" for="dreams-edit-public">Do you want your<wbr> dream to be public?</label>
            <input id="dreams-edit-public" class="dreams-edit-public" type="checkbox" name="dreams-edit-public">
          </div>
      </div>
    </div>
  </div>
  <div id="dreams-div-public-list" class="dreams-dream-list">
    <div>
      <h3>All Dreams</h3>
      <div class="dreams-page-selector">
        <p class="dreams-page-button" onclick="previousDreamPage(true);">Previous</p>
        <p class="dreams-page-info">Page <input id="public-dreams-page-text-input" class="dreams-page-text-input" onchange="publicDreamPageTextInput()" type="text"></input> of <span id="dreams-public-page-count"></span></p>
        <p class="dreams-page-button" onclick="nextDreamPage(true);">Next</p>
      </div>
    </div>
    <div id="dreams-public-dream-list" class="dreams-dream-list-dreams">

    </div>
  </div>
  <div id="dreams-div-statistics">
    <h3>Your Statistics</h3>
    <div>
      <div class="dreams-statistics-box">
        <svg id="color-dist-chart" viewBox="0 0 100 100" width="250px" height="250px">
        </svg>
        <p>Color Distribution</p>
      </div>
      <div class="dreams-statistics-box">
        <svg id="lucid-fraction-chart" viewBox="0 0 100 100" width="250px" height="250px">
        </svg>
        <p id="stats-dream-count"></p>
      </div>
      <div class="dreams-statistics-box">
        <p>Sleep time in the last 30 days</p>
        <svg id="sleep-time-chart" viewBox="0 0 600 240" width="600px" height="240px">

        </svg>
        <p id="stats-sleep-time"></p>
      </div>
      <div class="dreams-statistics-box">
        <p>Text length in the last 30 days</p>
        <svg id="text-length-chart" viewBox="0 0 600 240" width="600px" height="240px">

        </svg>
        <p id="stats-text-length"></p>
      </div>
    </div>
    <h3>Public Statistics</h3>
    <div>
      <div class="dreams-statistics-box">
        <p id="dreams-statistics-public-dreams" class="dreams-statistics-number"><p>
        <p>Public Dreams</p>
      </div>
      <div class="dreams-statistics-box">
        <p id="dreams-statistics-public-lucid-dreams" class="dreams-statistics-number"><p>
        <p>Public Lucid Dreams</p>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript" src="statistics.js"></script>
<script type="text/javascript" src="main.js"></script>
<?php
}
else{
  ?><div>Please log in at <a href="/login">https://johnnyst.de/login</a> to use Johnnys dream diary.<?php
}
  require($_SERVER['DOCUMENT_ROOT'].'/data/footer-page.php');
?>
