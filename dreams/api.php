<?php
session_start();
require_once($_SERVER['DOCUMENT_ROOT']."/data/config/dream_database.php");
$db = new DreamDatabase();

function edit_json($value)
{
  global $db;
  if(!isset($value['text'])){
    $value['text'] = "";
  }
  if(strlen($value['text'])>0 && !$value['public']){
    $value['text'] = $db->decrypt_text($value['text'], $_SESSION['password']);
  }
  $value['date'] = date("Y-m-d", $value['sleep_end']);
  $value['sleep_end'] = date("H:i", $value['sleep_end']);
  $value['sleep_start'] = date("H:i", $value['sleep_start']);
  return $value;
}

if(isset($_GET['user_dream_list'])){
  $json = $db->get_dreams($_GET['user_dream_list'], 20)->fetchAll(PDO::FETCH_ASSOC);
  foreach ($json as $key => $value) {
    $json[$key] = edit_json($value);
  }
  echo json_encode($json);
}
else if(isset($_GET['add_new_dream'])){
  echo $db->insert_new();
}
else if(isset($_GET['public_dream_list'])){
  $json = $db->get_all_dreams($_GET['public_dream_list'], 20)->fetchAll(PDO::FETCH_ASSOC);
  foreach ($json as $key => $value) {
    $json[$key] = edit_json($value);
  }
  echo json_encode($json);
}
else if(isset($_GET['save_dream'])){
  $db->xss($_POST);
  if(isset($_POST['data'])){
    $json = json_decode($_POST['data'], true);
    $db->edit($_GET['save_dream'], $json);
  }
}
else if(isset($_GET['get_public_dream_count'])){
  echo $db->get_public_dream_count();
}
else if(isset($_GET['get_dream_count'])){
  echo $db->get_dream_count();
}
else if(isset($_GET['get_dream'])){
  $json = $db->get_dream_by_number($_GET['get_dream']);
  $json = edit_json($json);
  echo json_encode($json);
}
else if(isset($_GET['delete_dream'])){
  $db->delete_dream($_GET['delete_dream']);
}
else if(isset($_GET['statistics'])){
  echo $db->statistics();
}

?>
