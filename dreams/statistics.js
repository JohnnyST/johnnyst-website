// Taken from https://stackoverflow.com/questions/43733975/how-to-animate-the-svg-path-for-an-arc#43749689
function polarToCartesian(centerX, centerY, radius, angleInDegrees) {
  var angleInRadians = (angleInDegrees-90) * Math.PI / 180.0;

  return {
    x: centerX + (radius * Math.cos(angleInRadians)),
    y: centerY + (radius * Math.sin(angleInRadians))
  };
}

function describeArc(x, y, radius, startAngle, endAngle){

    var start = polarToCartesian(x, y, radius, endAngle);
    var end = polarToCartesian(x, y, radius, startAngle);

    var arcSweep = endAngle - startAngle <= 180 ? "0" : "1";

    var d = [
        "M", start.x, start.y,
        "A", radius, radius, 0, arcSweep, "0", end.x, end.y,
        "L", x,y,
        "L", start.x, start.y
    ].join(" ");

    return d;
}

function updateStatistics(){
  const Http = new XMLHttpRequest();
  const url='api.php?statistics';
  Http.open("GET", url);
  Http.send();
  Http.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200){
        charts(JSON.parse(Http.responseText));
    }
  };
}

function bezierTimePath(values, maxValue){
  e = 0;
  path = '';
  for (var day in values) {
    if(e==0){
      path += '<path stroke="#f76b00" stroke-width="2" fill="none" d="M 600,'+(240-(values[day]/maxValue)*240);
    }
    else{
      path += ' C '+((30-(day-dBefore)/2)*20)+','+(240-(values[dBefore]/maxValue)*240)+' '+((30-(day-dBefore)/2)*20)+','+(240-(values[day]/maxValue)*240)+' '+((30-day)*20)+','+(240-(values[day]/maxValue)*240);
    }
    dBefore = day;
    e++;
  }
  return path+'" />';
}

function lastMonth(e){
  var dateToday = new Date().getTime();
  var times = new Array();
  for (var day in e){
      var d = Math.floor((dateToday-new Date(day*1000).getTime())/86400000);
      if(d>=0&&d<30){
        if(times[d]>0){
          times[d] = (e[day]+times[d])/2;
        }
        else{
          times[d] = e[day];
        }
      }
  }
  return times;
}

function charts(json){
  var sum = Number(json['count'])+Number(json['count-lucid']);

  document.getElementById("dreams-statistics-public-dreams").innerHTML = Number(json['public-count'])+Number(json['public-count-lucid']);
  document.getElementById("dreams-statistics-public-lucid-dreams").innerHTML = json['public-count-lucid'];

  lucidFraction = document.getElementById("lucid-fraction-chart");
  lucidFraction.innerHTML = "";
  lucidFraction.innerHTML += '<path fill="#f76b00" stroke-width="0" d="'+describeArc(50, 50, 50, 0, (Number(json['count-lucid'])) * 360/sum)+'" />';
  lucidFraction.innerHTML += '<path fill="#a02800" stroke-width="0" d="'+describeArc(50, 50, 50, (Number(json['count-lucid'])) * 360/sum, 360)+'" />';
  document.getElementById("stats-dream-count").innerHTML = '<span style="color: #a02800">'+json['count']+' Non-Lucid Dreams</span>, <span style="color: #f76b00">'+json['count-lucid']+'<img class="dreams-icon" src="lucid.svg"> Lucid Dreams</span>';
  colorDist = document.getElementById("color-dist-chart");
  colorDist.innerHTML = "";
  var e = 0;
  for(color in json['color-dist']) {
    if(Object.keys(json['color-dist']).length == 1){
      colorDist.innerHTML = '<circle cx="50" cy="50" r="50" fill="'+color+'" />';
      break;
    }
    colorDist.innerHTML += '<path fill="'+color+'" stroke-width="0" d="'+describeArc(50, 50, 50, e * 360/sum, (e+Number(json['color-dist'][color])) * 360/sum)+'" />';
    e += Number(json['color-dist'][color]);
  }

  sleepTime = document.getElementById("sleep-time-chart");
  sleepTime.innerHTML = "";
  sleepTime.innerHTML += bezierTimePath(lastMonth(json['sleep_length']), 86400);
  for (var i = 0; i < 6; i++) {
    sleepTime.innerHTML += '<text fill="black" x="0" y="'+(240-i*40)+'">'+(i*4)+'</text>'
    sleepTime.innerHTML += '<line stroke="black" stroke-width="0.5" x1="20" y1="'+(239-i*40)+'" x2="600" y2="'+(239-i*40)+'" />'
  }

  textLength = document.getElementById("text-length-chart");
  textLength.innerHTML = "";
  var textLengths = lastMonth(json['text_length']);
  var l = 0;
  for (var t in textLengths) {
    if(textLengths[t] > l)
      l = Number(textLengths[t]);
   }
   console.log(textLengths);
   console.log(l);
  textLength.innerHTML += bezierTimePath(textLengths, l);
  for (var i = 0; i < 5; i++) {
    textLength.innerHTML += '<text fill="black" x="0" y="'+(240-i*48)+'">'+(i*(l/6))+'</text>'
    textLength.innerHTML += '<line stroke="black" stroke-width="0.5" x1="20" y1="'+(239-i*48)+'" x2="600" y2="'+(239-i*48)+'" />'
  }

  document.getElementById("stats-sleep-time").innerHTML = 'Average Sleep Time: '+Math.floor(Number(json['avg-sleep-time'])/3600)+' Hours, '+Math.floor((Number(json['avg-sleep-time'])%3600)/60)+' Minutes';
}
