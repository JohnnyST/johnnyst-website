function addLeadingZeros(n) {
  if (n <= 9) {
    return "0" + n;
  }
  return n
}

function initInputs(){
  document.getElementById("dreams-edit-title").value = "";
  document.getElementById("dreams-edit-text").value = "";
  var d = new Date();
  document.getElementById("dreams-edit-date").value = d.getFullYear()+"-"+addLeadingZeros(d.getMonth()+1)+"-"+d.getDate();
  document.getElementById("dreams-edit-sleep-end").value = addLeadingZeros(d.getHours())+":"+addLeadingZeros(d.getMinutes());
  d = new Date(Date.now()-28800000);
  document.getElementById("dreams-edit-sleep-start").value = addLeadingZeros(d.getHours())+":"+addLeadingZeros(d.getMinutes());
  document.getElementById("dreams-edit-color").value = "#d37b17";
  setColor();
  document.getElementById("dreams-edit-lucid").checked = false;
  document.getElementById("dreams-edit-nightmare").checked = false;
  document.getElementById("dreams-edit-favourite").checked = false;
  document.getElementById("dreams-edit-public").checked = false;
}

function fillInputs(json){
  document.getElementById("dreams-edit-title").value = json["title"];
  document.getElementById("dreams-edit-text").value = json["text"];
  document.getElementById("dreams-edit-date").value = json["date"];
  document.getElementById("dreams-edit-sleep-end").value = json["sleep_end"];
  document.getElementById("dreams-edit-sleep-start").value = json["sleep_start"];
  document.getElementById("dreams-edit-color").value = json["color"];
  setColor();
  document.getElementById("dreams-edit-lucid").checked = (json["lucid"]>0);
  document.getElementById("dreams-edit-nightmare").checked = (json["nightmare"]>0);
  document.getElementById("dreams-edit-favourite").checked = (json["favourite"]>0);
  document.getElementById("dreams-edit-public").checked = (json["public"]>0);
}

function setColor(){
  document.getElementById("dreams-div-edit-dream").style = 'border-color: '+document.getElementById("dreams-edit-color").value+';';
}

function saveChanges(){
  if(editingDream != -1){
    if(document.getElementById("dreams-edit-color").value.length == 0){
      document.getElementById("dreams-edit-color").value = "#d37b17";
    }
    var d = {};
    d["title"] = document.getElementById("dreams-edit-title").value;
    d["text"] = document.getElementById("dreams-edit-text").value;
    d["date"] = document.getElementById("dreams-edit-date").value;
    if(document.getElementById("dreams-edit-sleep-start").value.length > 0){
      d["sleep_start"] = document.getElementById("dreams-edit-sleep-start").value;
    }
    if(document.getElementById("dreams-edit-sleep-end").value.length > 0){
      d["sleep_end"] = document.getElementById("dreams-edit-sleep-end").value;
    }
    d["public"] = document.getElementById("dreams-edit-public").checked;
    d["lucid"] = document.getElementById("dreams-edit-lucid").checked;
    d["nightmare"] = document.getElementById("dreams-edit-nightmare").checked;
    d["favourite"] = document.getElementById("dreams-edit-favourite").checked;
    d["color"] = document.getElementById("dreams-edit-color").value;
    const Http = new XMLHttpRequest();
    const url='api.php?save_dream='+editingDream;
    Http.open("POST", url, true);
    Http.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    Http.send("data="+JSON.stringify(d));
    Http.onreadystatechange = function() {
      if(this.readyState == 4 && this.status == 200){
        console.log(Http.responseText);
      }
    };
  }
}

function requestUserDreams(){
  const Http = new XMLHttpRequest();
  const url='api.php?user_dream_list='+dreamListPage;
  Http.open("GET", url);
  Http.send();
  Http.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200){
      updateDreamList(JSON.parse(Http.responseText), false);
    }
  };
}
function updateDreamList(jsonDreamList, public){
  var e = null;
  if(public){
    e = document.getElementById("dreams-public-dream-list");
  }
  else{
    e = document.getElementById("dreams-user-dream-list");
  }
  e.innerHTML = "";
  jsonDreamList.forEach((dream) => {
    if(public){
      e.innerHTML += '<div class="dreams-dream-list-dream" style="border-color: '+dream['color']+';" ><div><h4>'+dream['title']+'</h4><span class="dream-list-dream-icons">'+(dream['lucid']==1?'<img class="dreams-icon" src="lucid.svg">':'')+(dream['favourite']==1?'<img class="dreams-icon" src="favourite.svg">':'')+(dream['nightmare']==1?'<img class="dreams-icon" src="nightmare.svg">':'')+'</span><span>'+dream['date']+'</span></div><p class="dream-list-text">'+dream['text']+'</p><p class="dream-list-username">'+dream['username']+'</p></div>';
    }else{
      e.innerHTML += '<div class="dreams-dream-list-dream" style="cursor:pointer;border-color: '+dream['color']+';" onclick="editDream('+dream['users_number']+')"><div><h4>'+dream['title']+'</h4><span class="dream-list-dream-icons">'+(dream['lucid']==1?'<img class="dreams-icon" src="lucid.svg">':'')+(dream['favourite']==1?'<img class="dreams-icon" src="favourite.svg">':'')+(dream['nightmare']==1?'<img class="dreams-icon" src="nightmare.svg">':'')+'</span><span>'+dream['date']+'</span></div><p class="dream-list-text">'+dream['text']+'</p></div>';
    }
  });
}

function editDream(users_number){
  const Http = new XMLHttpRequest();
  const url='api.php?get_dream='+users_number;
  Http.open("GET", url);
  Http.send();
  Http.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200){
      editingDream = users_number;
      setPage(1);
      setNav(-1);
      initInputs();
      fillInputs(JSON.parse(Http.responseText));
    }
  };
}

function setNav(n){
  for (var i = 0; i < navButtons.length; i++) {
    if(i==n){
      navButtons.item(i).classList.add("dreams-navigation-selected");
    }
    else{
      navButtons.item(i).classList.remove("dreams-navigation-selected");
    }
  }
}
function setPage(page){
    if(currentPage==page)return;
    pages[page].classList.remove("dream-div-hidden");
    pages[page].classList.add("dream-div-visible");
    if(currentPage!=-1){
      pages[currentPage].classList.add("dream-div-hidden");
      pages[currentPage].classList.remove("dream-div-visible");
    }
    currentPage = page;
}

function toDreamDiary(){
  if(editingDream != -1){
    saveChanges();
  }
  updateDreamPages();
  requestUserDreams();
  setNav(0);
  setPage(0);
  editingDream = -1;
}

function requestInsertNewDream(){
  const Http = new XMLHttpRequest();
  const url='api.php?add_new_dream';
  Http.open("GET", url);
  Http.send();
  Http.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200){
      editingDream = Http.responseText;
      initInputs();
      setPage(1);
      setNav(1);
    }
  };
}
function addNewDream(){
  if(editingDream != -1){
    saveChanges();
  }
  requestInsertNewDream();
}
function toStatistics(){
  if(editingDream != -1){
    saveChanges();
  }
  updateStatistics();
  setNav(3);
  setPage(3);
  editingDream = -1;
}
function toPublicDreams(){
  if(editingDream != -1){
    saveChanges();
  }
  updateDreamPages();
  requestPublicDreams();
  setNav(2);
  setPage(2);
  editingDream = -1;
}
function updatePageButtons(){
  if(publicDreamListPage==0){
    document.getElementsByClassName('dreams-page-button')[2].classList.add('disabled');
  }
  else{
    document.getElementsByClassName('dreams-page-button')[2].classList.remove('disabled');
  }
  if(dreamListPage==0){
    document.getElementsByClassName('dreams-page-button')[0].classList.add('disabled');
  }
  else{
    document.getElementsByClassName('dreams-page-button')[0].classList.remove('disabled');
  }
  if(publicDreamListPage==publicDreamPages-1){
    document.getElementsByClassName('dreams-page-button')[3].classList.add('disabled');
  }
  else{
    document.getElementsByClassName('dreams-page-button')[3].classList.remove('disabled');
  }
  if(dreamListPage==dreamPages-1){
    document.getElementsByClassName('dreams-page-button')[1].classList.add('disabled');
  }
  else{
    document.getElementsByClassName('dreams-page-button')[1].classList.remove('disabled');
  }
}

function deleteDream(){
  if(editingDream == -1)return;
  const Http = new XMLHttpRequest();
  const url='api.php?delete_dream='+editingDream;
  Http.open("GET", url);
  Http.send();
  Http.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200){
      editingDream = -1;
      toDreamDiary();
    }
  };
}
function updateDreamPages(){
  const Http = new XMLHttpRequest();
  var url='api.php?get_dream_count';
  Http.open("GET", url);
  Http.send();
  Http.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200){
      dreamPages = Math.floor(Number(Http.responseText)/20)+1;
      document.getElementById('dreams-page-count').innerHTML = dreamPages;
      updatePageButtons();
    }
  };
  const Http2 = new XMLHttpRequest();
  url='api.php?get_public_dream_count';
  Http2.open("GET", url);
  Http2.send();
  Http2.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200){
      publicDreamPages = Math.floor(Number(Http2.responseText)/20)+1;
      document.getElementById('dreams-public-page-count').innerHTML = publicDreamPages;
      updatePageButtons();
    }
  };
}
function requestPublicDreams(){
  const Http = new XMLHttpRequest();
  const url='api.php?public_dream_list='+publicDreamListPage;
  Http.open("GET", url);
  Http.send();
  Http.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200){
      updateDreamList(JSON.parse(Http.responseText), true);
    }
  };
}
function dreamPageTextInput(){
  var p = Number(document.getElementById('dreams-page-text-input').value);
  if(Number.isFinite(p)){
    setDreamPage(p-1, false);
  }
}
function publicDreamPageTextInput(){
  var p = Number(document.getElementById('public-dreams-page-text-input').value);
  if(Number.isFinite(p)){
    setDreamPage(p-1, false);
  }
}
function setDreamPage(page, public){
  if(page>=0){
    if(public){
      if(page<=publicDreamPages){
        publicDreamListPage = page;
        document.getElementById('public-dreams-page-text-input').value = page+1;
        updatePageButtons();
        requestPublicDreams();
      }
    }
    else{
      if(page<=dreamPages){
        dreamListPage = page;
        document.getElementById('dreams-page-text-input').value = page+1;
        updatePageButtons();
        requestUserDreams();
      }
    }
  }
}
function previousDreamPage(public){
  if(public){
    if(publicDreamListPage>0){
      setDreamPage(publicDreamListPage-1, public);
    }
  }
  else{
    if(dreamListPage>0){
      setDreamPage(dreamListPage-1, public);
    }
  }
}
function nextDreamPage(public){
  if(public){
    if(publicDreamListPage<publicDreamPages-1){
      setDreamPage(publicDreamListPage+1, public);
    }
  }
  else{
    if(dreamListPage<dreamPages-1){
      setDreamPage(dreamListPage+1, public);
    }
  }
}

var currentPage = -1;
var dreamListPage = 0;
var dreamPages = 1;
var publicDreamListPage = 0;
var publicDreamPages = 1;
var editingDream = -1;
var pages = [document.getElementById("dreams-div-user-list"), document.getElementById("dreams-div-edit-dream"), document.getElementById("dreams-div-public-list"), document.getElementById("dreams-div-statistics")];
var navButtons = document.getElementsByClassName("dreams-navigation-button");
updateDreamPages();
setDreamPage(0, true);
setDreamPage(0, false);
toDreamDiary();
