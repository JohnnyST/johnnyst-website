<!DOCTYPE html>
<?php
session_start();
require_once($_SERVER['DOCUMENT_ROOT']."/data/config/database.php");
class SongVoteDatabase extends Database{
  private $table = 'dreams';

  public function __construct(){
    parent::__construct("data");
  }
  public function get_elo($song_id){
    return $this->prepared_statement("SELECT elo FROM songs WHERE id = ?", array($song_id))->fetch()['elo'];
  }
  public function set_elo($song_id, $elo){
    return $this->prepared_statement("UPDATE songs SET elo=? WHERE id = ?", array($elo, $song_id));
  }
  // p: points, 0 if b won, 0.5 if remis, 1 if a won
  public function vote($p, $a, $b){
    $elo_a = $this->get_elo($a);
    $elo_b = $this->get_elo($b);
    $e = 1/(1+pow(10, ($elo_b-$elo_a)/600));
    $this->set_elo($a, $elo_a+10*($p-$e));
    $this->set_elo($b, $elo_b-10*($p-$e));
    $this->prepared_statement("INSERT INTO song_vote_log ".'(timestamp,p,song_1,song_2) VALUES ('.date ('Y-m-d H:i:s', mktime()).', ?, ?, ?)', array($p,$a,$b));
  }
  public function get_album($song_id){
    return $this->prepared_statement("SELECT albums.name FROM albums JOIN songs_on_album ON songs_on_album.album_id=albums.id
      WHERE songs_on_album.song_id=? ORDER BY albums.priority", array($song_id))->fetch();
  }
  public function select_song(){
    $d = $this->prepared_statement("SELECT songs.id, songs.name FROM songs
      JOIN songs_on_album ON songs.id=songs_on_album.song_id
      JOIN albums ON albums.id=songs_on_album.album_id
            JOIN artists ON albums.artist=artists.id WHERE artists.name='AC/DC'
       ORDER BY RAND() LIMIT 1", array())->fetch();
       return $d;
  }
}
$db = new SongVoteDatabase();

if (isset($_POST['submit'])&&isset($_GET['p'])&&isset($_GET['s_1'])&&isset($_GET['s_2'])) {
  $db->vote($_GET['p'],$_GET['s_1'],$_GET['s_2']);
}

$song_1 = $db->select_song();
$song_2 = $db->select_song();
while ($song_1['id']==$song_2['id']) {
  $song_2 = $db->select_song();
}
if(isset($_POST['submit-terms']))$_SESSION['agree_terms'] = true;
 ?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>AC/DC Song Vote</title>
    <meta property="og:type" content="website">
<meta property="og:title" content="In Rock We Trust">
<meta property="og:description" content="Song Vs Song">
<meta content="#e60202" data-react-helmet="true" name="theme-color">
<meta property="og:image" content="https://media.giphy.com/media/LCfM5hrqBKPlydyyYR/giphy.gif">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@discord">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Teko:wght@300;500;700&display=swap" rel="stylesheet">
    <style>
    html, body{
      width: 100%;
      height: 100%;
      margin: 0;
      background-color: #000;
      color: #fff;
      font-family: 'Teko', sans-serif;
    }
    .results{
      text-align: center;
    }
    .results > *{
      margin: 1em;
      display: flex;
      justify-content: center;
      align-items: flex-start;
    }
    .results table{
      border: 1px solid #fff;
    }
    .results td{
      padding: 0.3em;
    }
    .info-popup{
      text-align: center;
    }
    .info-popup > *{
      margin: 0 auto;
      max-width: 800px;
    }
    .info-title{
      font-size: 4em;
      font-weight: bold;
    }
    .info-text{
      font-size: 1.5em;
      font-weight: 500;
    }
    .info-text a{
      color: #fbb;
    }
    .info-text a:visited{
      color: #fbb;
    }
    .submit-terms{
      font-size: 1.8em;
      font-weight: 700;
      color: #fff;
      border: 1px solid #fff;
      border-radius: 2px;
      background-color: #000;
      padding: 0.3em 0.5em;
      cursor: pointer;
    }
    .title{
      font-size: 4em;
      font-weight: bold;
      text-align: center;
      width: 100%;
      margin: 0.2em 0;
    }
    .main-form{
      display: flex;
      align-items: center;
      justify-content: center;
    }
    .votes{
      display: flex;
      justify-content: space-around;
      align-items: stretch;
    }
    .vote{
      margin: 4vh 4vw;
      flex: 1 0 0;
      position: relative;
      text-align: center;
      align-items: center;
      justify-content: center;
      flex-direction: column;
      display: flex;
    }
    .cover{
      max-width: 50%;
      max-height: 50%;
      padding: 1em;
      z-index: 2;
      position: relative;
      display: block;
    }
    .submit, .background{
      width: 100%;
      height: 100%;
      position: absolute;
      top: 0;
      left: 0;
      background: none;
      border: none;
    }
    .submit{
      cursor: pointer;
      z-index: 5;
    }
    .song-name{
      z-index: 3;
      position: relative;
      text-align: center;
      color: #fff;
      text-shadow: #000 2px 2px 2px;
      font-size: 3em;
      font-weight: bold;
      margin: 0.1em;
    }
    .background{
      z-index: 0;
      background-size: cover;
      background-position: center;
      filter: blur(5px) brightness(0.6);
      transition: filter 0.4s ease;

    }
    .submit:hover + .background{
      filter: blur(5px) brightness(0.8);
    }
    .vs-text{
      font-size: 5em;
      font-weight: bold;
      display: flex;
      align-items: center;
      text-shadow: #000 2px 2px 2px;
    }
    .secondary-buttons{
      display: flex;
      justify-content: center;
      width: 100%;
      align-items: center;
    }
    .secondary-buttons > form{
      width: 100%;
      flex-grow: 1;
    }
    .equal, .skip{
      padding: 0.5em;
      margin: 0.5em;
    }
    .equal{
      text-align: right;
    }
    .equal > input{
      font-size: 2.5em;
      padding: 0.5em 0.7em;
      background-color: #000;
      cursor: pointer;
      color: #fff;
      border: 1px solid #fff;
      transition: background-color 0.2s ease, color 0.2s ease;
    }
    .equal > input:hover{
    background-color: #fff;
    color: #000;
    }
    .skip{
    text-align: left;
    }
    .skip > input{
      font-size: 1.5em;
      padding: 0.5em 0.7em;
      background: none;
      color: #aaa;
      cursor: pointer;
      border: none;
    }
    .skip > input:hover{
    text-decoration: underline;
    }
    </style>
  </head>
  <body>
  <?php
  if (isset($_GET['results'])){
    echo '<div class="results" style="display: flex;align-items: top;justify-content: space-around">';
  $d = $db->prepared_statement("SELECT DISTINCT songs.elo, songs.name FROM songs
    JOIN songs_on_album ON songs.id=songs_on_album.song_id
    JOIN albums ON albums.id=songs_on_album.album_id
          JOIN artists ON albums.artist=artists.id WHERE artists.name='AC/DC'
     ORDER BY elo DESC", array());
     echo '<div style="flex: 1 1 0;"><table><tr><td>Song</td><td>ELO</td></tr>';
     foreach ($d->fetchAll() as $song) {
     echo "<tr><td>".$song['name']."</td><td>".$song["elo"]."</td></tr>";
     }
     echo "</table></div>";?>
     <div style="flex: 1 1 0;">
       <div>
       <h1 class="info-title">Results</h1>
       <div class="info-text">
         <p>These are the live results of the AC/DC song ELO vote. The songs began with a ELO of 1000.</p>
         <p>On the left, there is a table with all the songs and their ELO, ordered by ELO.</p>
         <p>On the right, there is a table with the albums/compilations and the average ELO of their songs
           as well as the standard deviation of the ELO, ordered by average ELO. (The greater the standard deviation of the album,
           the more do the songs differ in ELO.)</p>
      </div>
    </div>
    </div>

     <?php
     $d = $db->prepared_statement("SELECT DISTINCT albums.name, AVG(songs.elo) AS avg, STD(songs.elo) AS std FROM songs
       JOIN songs_on_album ON songs.id=songs_on_album.song_id
       JOIN albums ON albums.id=songs_on_album.album_id
             JOIN artists ON albums.artist=artists.id WHERE artists.name='AC/DC' GROUP BY albums.id
        ORDER BY AVG(songs.elo) DESC", array());
        echo '<div style="flex: 1 1 0;"><table><tr><td>Album</td><td>AVG(ELO)</td><td>STD(ELO)</td></tr>';
        foreach ($d->fetchAll() as $album) {
        echo "<tr><td>".$album['name']."</td><td>".round($album["avg"])."</td><td>".round($album["std"])."</td></tr>";
        }
        echo "</table></div>";

       echo '</div>';
   }
  else if(!isset($_SESSION['agree_terms'])){ ?>
    <div class="info-popup">
      <h1 class="info-title">Welcome!</h1>
      <div class="info-text">
        <p>Hello, dear fellow AC/DC fan. This site was set up to ultimately and objectively (cough) find out what the best AC/DC song is.
        I implemented an ELO-like algorithm, so you will see two songs and have to decide which of them is the better.
        This will eventually lead to the creation of a ranking of all the best songs of the best band of all time.</p>
        <p>In a non-zero, but finite amount of time I will analyze the results and share them.
          To get notified, join the In Rock We Trust AC/DC discord server (<a href="https://discord.gg/kz47Myzq5z" target="_blank">https://discord.gg/kz47Myzq5z</a>)
        and get the server announcements role.</p>
        <p>By continuing to use this site, clicking the button below and voting, you agree to the storage of the results of your votes.
        </p>
        <p>Very much thank you for participating in this little project.</p>
        <form action="songs.php" method="post"><input class="submit-terms" type="submit" name="submit-terms" value="I agree" /></form>
      </div>
    </div>
  <?php } else{ ?>
    <div class="main-form">
      <div>
        <h1 class="title">Which AC/DC song is better?</h1>
        <div class="votes">
          <form class="vote" action="songs.php?<?php echo 'p=1&s_1='.$song_1['id'].'&s_2='.$song_2['id']; ?>" method="post" onclick="submit">
            <input class="submit" type="submit" name="submit" value="">
            <div class="background" style="background-image: url('<?php echo 'covers/'.str_replace('\'', '', str_replace('.', '', str_replace(' ', '', strtolower($db->get_album($song_1['id'])['name'])))).".jpg"; ?>');"></div>
            <img class="cover" src="<?php echo 'covers/'.str_replace('\'', '', str_replace('.', '', str_replace(' ', '', strtolower($db->get_album($song_1['id'])['name'])))).".jpg"; ?>" />
            <p class="song-name"><?php echo $song_1['name']; ?></p>
          </form>
          <div class="vs-text"><p>VS</p></div>
          <form class="vote" action="songs.php?<?php echo 'p=0&s_1='.$song_1['id'].'&s_2='.$song_2['id']; ?>" method="post" onclick="submit">
            <input class="submit" type="submit" name="submit" value="">
            <div class="background" style="background-image: url('<?php echo 'covers/'.str_replace('\'', '', str_replace('.', '', str_replace(' ', '', strtolower($db->get_album($song_2['id'])['name'])))).".jpg"; ?>');"></div>
            <img class="cover" src="<?php echo 'covers/'.str_replace('\'', '', str_replace('.', '', str_replace(' ', '', strtolower($db->get_album($song_2['id'])['name'])))).".jpg"; ?>" />
            <p class="song-name"><?php echo $song_2['name']; ?></p>
          </form>
        </div>
        <div class="votes">
          <div class="secondary-buttons">
            <form class="equal" action="songs.php?<?php echo 'p=0.5&s_1='.$song_1['id'].'&s_2='.$song_2['id']; ?>" method="post">
              <input type="submit" name="submit" value="Equal" />
            </form>
            <form class="skip" action="songs.php" method="post">
              <input type="submit" name="submit" value="Skip" />
            </form>
          </div>
        </div>
      </div>
    </div>
  <?php } ?>
  </body>
</html>
