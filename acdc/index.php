<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>In Rock We Trust - AC/DC Fan Community</title>
    <link rel="stylesheet" href="/css/acdc.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Saira+Stencil+One&display=swap" rel="stylesheet">
  </head>
  <body>
    <div id="hero-banner">
      <div id="hero-lightning"></div>
      <div id="headline">In Rock We Trust</div>
      <div id="discord-widget"><iframe src="https://discord.com/widget?id=771094006988931073&theme=dark" width="350" height="500" allowtransparency="true" frameborder="0" sandbox="allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts"></iframe></div>
    </div>

    <footer><a href="/notice">Site Notice</a>
    <a href="/privacypolicy">Privacy Policy</a></footer>
  </body>
</html>
