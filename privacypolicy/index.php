<?php
  $title = "JohnnyST - Privacy Policy";
  $nav = 0;
  require($_SERVER['DOCUMENT_ROOT']."/data/header-page.php");
?>
<div class="datenschutzerklaerung">
  <h1>Datenschutzerklärung</h1>
  <p> Diese Datenschutzerklärung klärt die Besucher/Nutzer meiner Website, die Zuschauer meiner Videos auf dem Videoportal YouTube und meine direkten Bekannten, Freunde und Familienmitglieder (im Folgenden &bdquo;Nutzer&ldquo; genannt) über die Art, den Umfang und Zweck der Verarbeitung/Speicherung von personenbezogenen Daten und Fotografien aus privaten, hobbymäßigen oder erwünschten Gründen (nachfolgend kurz &bdquo;Daten&ldquo;) innerhalb des gesamten Umgangs mit meiner Person, meiner Website(s) und meiner Social-Media-Profile auf (Im Folgenden kurz &bdquo;Angebot&ldquo; bzw. &bdquo;Onlineangebot&ldquo; bei ausschließlich auf das WorldWideWeb bezogenen Kontexten). </p>
  <h2>Verantwortlicher</h2>
  <p> Jonas, Strotmann / Privatperson<br>Ringstraße 10<br>46342 Velen<br>Deutschland<br>admin@johnnyst.de<br>Impressum: <a href="/notice">https://johnnyst.de/notice</a>
  </p>
  <h2>Datenspeicherung beim Onlineangebot des Verantwortlichen</h2>
  <h3>Die Login-Funktion</h3>
  <p>Die Login-Funktion speichert zunächst nur den eingegebenen Nutzernamen. Das gewählte Passwort wird lediglich in gehashter und gesalzener Form in der Datenbank gespeichert, um zu gewährleisten, dass nur der Nutzer dieses kennt.</p>
  <h3>Das Traumtagebuch</h3>
  <p>Die im Traumtagebuch eingegebenen Daten werden ebenfalls in einer Datenbank gespeichert. Dort sind sie nicht für Dritte einsehbar und werden keinesfalls an solche weitergegeben. Für den Betreiber der Seite sind lediglich die Titel der Träume sowie die jeweiligen Angaben zu Datum und Uhrzeit. <b>Die Texte werden mit dem Zugangspasswort des Nutzers mithilfe des symmetrischen Verschlüsselungsverfahrens AES-256-CBC verschlüsselt und sind daher auch für den Betreiber nicht lesbar.</b> Dies ändert sich nur dann, wenn der Nutzer bei der Eingabe seines Traums die Checkbox aktiviert, die ihn fragt, ob er seinen Traum öffentlich zugänglich speichern möchte. In diesem Fall ist der Text für jeden einsehbar, der ebenfalls einen Account auf dieser Website hat. Ich kann keine Garantie für das längerfristige Fortbestehen der Daten auf der Seite geben.</p>
  <h2>Datenschutz-/Urheberrechts&shy;vereinbarungen in Bezug auf Bild- und Tonmaterialien</h2>
  <p> Dieser Abschnitt behandelt vom Verantwortlichen aufgenommene Bild- und Tonmaterialien und ist daher ausschließlich für direkte Bekannte des Verantwortlichen relevant.</p>
  <h3>Speicherung</h3>
  <p>Bild- und Tonmaterialien (nachfolgend als &bdquo;Materialien&ldquo; bezeichnet), auf denen eine oder mehrere bekannte oder unbekannte Personen auftreten, werden nur mit ausdrücklicher Zustimmung der Person(en), was eine Akzeptanz der Datenschutzerklärung einschließt, erfasst. Die Materialien werden gewöhnlich unberührt für eine unbestimmte Zeit auf einer Festplatte gespeichert, sofern es nicht beabsichtigt oder unbeabsichtigt zu Löschungen wegen Unfällen oder niedrigen Speicherplatzes kommt. Eine Garantie für das längerfristige Fortbestehen der Materialien kann nicht gewährleistet werden. Die in den Materialien aufgenommenen Personen haben jedoch selbstverständlich das Recht, eine Kopierung oder Übernahme zwecks Selbstverwendung oder eine Überschreibung der Materialien zu veranlassen. Bei einer Kopierung gehen die Materialien jedoch nicht in den Besitz der Person über. Stattdessen gilt für die Materialien immer noch diese Datenschutzerklärung und der Verantwortliche besitzt weiterhin ein teilweises Urheberrecht an den Materialien.</p>
  <h3>Veröffentlichung</h3>
  <p>Die Veröffentlichung der Materialien wird, wenn überhaupt, nur nach Besprechung des Zwecks, des Ortes, der Art und Weise, des Zeitraumes und der urheberrechtlichen Lage der Veröffentlichung mit ausdrücklicher Zustimmung erfolgen. Auch hier dürfen die betroffenen Personen natürlich eine sofortige Löschung der Materialien aus dem Internet oder anderen öffentlichen Medien veranlassen. Es kann jedoch keinerlei Haftung für Downloads oder Kopien der Materialien während des Zeitraumes der Veröffentlichung übernommen werden. Hier sind die Personen, die die Materialien (widerrechtlich) heruntergeladen oder anderwertig kopiert haben, verantwortlich. </p>
</div>
<?php
  require($_SERVER['DOCUMENT_ROOT'].'/data/footer-page.php');
?>
