<?php
  $title = "JohnnyST - Login";
  $nav = 3;
  require($_SERVER['DOCUMENT_ROOT']."/data/header-page.php");
  $db = new LoginDatabase();
  $code = -1;
  if(isset($_POST['submit'])){
    $code = $db->validate_login($db->xss($_POST));
  }
?>
<div>
  <p>Not signed up yet? Create an account at <a href="/signup">https://johnnyst.de/signup</a>.</p>
  <?php if($code!=0){ ?>
<form class="" action="." method="post">
<label for="name">Username:</label><input type="text" name="name"><br>
<label for="password">Password:</label><input type="password" name="password"><br>
<input type="submit" name="submit" value="Login">
</form>
<?php }
  if($code==0){
    echo "<p>Login successful.</p>";
  }
  else if($code==1){
    echo "<p>Please enter your username.</p>";
  }
  else if($code==2){
    echo "<p>Please enter your password.</p>";
  }
  else if($code==3){
    echo '<p>That user does not exist. You can create an account at <a href="/signup">https://johnnyst.de/signup</a>.</p>';
  }
 ?>
</div>
<?php
  require($_SERVER['DOCUMENT_ROOT'].'/data/footer-page.php');
?>
