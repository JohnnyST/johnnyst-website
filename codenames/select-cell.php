<?php
session_start();
if(isset($_GET['code'])){
  require_once($_SERVER['DOCUMENT_ROOT']."/data/config/codenames_database.php");
  $database = new CodenamesDatabase();
  $database->xss($_GET);
    if($database->game_exists($_GET['code']) && $database->get_winner($_GET['code']) == null){
      if($database->get_team(session_id())==$database->current_turn($_GET['code'])){
      if($database->is_explainer(session_id())){
        if($database->selected_cells_count($_GET['code'])<3
        && $database->get_team(session_id())==$database->cell_team($_GET['code'], $_GET['x'], $_GET['y']))
        $database->select_cell($_GET['code'], $_GET['x'], $_GET['y']);
      }
      else{
        if($database->current_turn($_GET['code'])==$database->get_team(session_id())){
          $t = $database->cell_team($_GET['code'], $_GET['x'], $_GET['y']);
          $database->reveal_cell($_GET['code'], $_GET['x'], $_GET['y'], session_id());
          if($t=='o'){
            $database->bomb_hit($_GET['code'], session_id());
          }
          else {
            if($database->selected_cells_count($_GET['code'])==0){
              $database->next_turn($_GET['code']);
            }
            else if($t==$database->get_team(session_id())){
            }
            else{
              $database->next_turn($_GET['code']);
            }
          }
        }
      }}
    }
}
 ?>
