<?php
require_once($_SERVER['DOCUMENT_ROOT']."/data/config/codenames_database.php");
$database = new CodenamesDatabase();
$database->xss($_POST);
$database->xss($_GET);
$code = $_GET['code'];
if($database->game_exists($code)){
echo "<h4>Game Code: $code </h4><p>";
if($database->get_winner($code)=='r'){
  echo "Red has won the game!";
}
else if($database->get_winner($code)=='b'){
  echo "Blue has won the game!";
}
else if ($database->current_turn($code)=='r') {
  echo "Red's turn";
}
else{
  echo "Blue's turn";
}
echo "</p>";
?>
<table>
  <tr><td colspan="2">Players</td></tr>

<tr>
  <td>Team Red</td>
  <td>Team Blue</td>
</tr>
<tr>
<?php
$p = $database->get_players($code, "r");
$p2 = $database->get_players($code, "b");
for ($i=0; $i < $p->rowCount(); $i++) {
  $r = $p->fetch();
  echo "<td>".$r['name'];
  if($r['explainer']==1)echo " (Explainer)";
  echo "</td>";
    $r = $p2->fetch();
    echo "<td>".$r['name'];
    if($r['explainer']==1)echo " (Explainer)";
    echo "</td>";
}
 ?>
</tr>
</table>
<br>
<table class="game-table">
<?php
  $st = $database->get_cells($code);
  $size = $database->get_size($code);
  $k = 0;
  for ($i=0; $i < $size['sizeY']; $i++) {
    echo "<tr>";
    for ($j=0; $j < $size['sizeX']; $j++) {
      $a = $st->fetch();
      echo '<td class="codenames-table-cell"';
      if($a['revealed']==1){
        echo ' class=cell-team-'.$a['team'];
      }
      echo '>'.$a["word"].'</td>';
      $k++;
    }
    echo "</tr>";
  }
?>
</table>
<?php
}

?>
