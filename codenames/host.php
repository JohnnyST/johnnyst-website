<?php
  $title = "Codenames - Host Game";
  $nav = 2;
  require($_SERVER['DOCUMENT_ROOT']."/data/header-page.php");
?>
<link rel="stylesheet" href="/css/codenames.css">
<div class="">
<?php
$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
$code = "";
for ($i=0; $i < 6; $i++) {
  $code = $code.$chars[mt_rand(0, strlen($chars) - 1)];
}
if(isset($_GET['sizeX']))$sizeX=$_GET['sizeX'];
else $sizeX = 5;
if(isset($_GET['sizeY']))$sizeY=$_GET['sizeY'];
else $sizeY = 5;

?>
<div>
  <h3>Enter the words for the game</h3>
  <h4>Game Code: <input type="text" name="gameCode" value="<?php echo $code; ?>"></h4>
  <form class="" action="master.php?<?php echo "code=$code&sizeX=$sizeX&sizeY=$sizeY" ?>" method="post">
<table>
<?php
  for ($i=0; $i < $sizeY; $i++) {
    echo "<tr>";
    for ($j=0; $j < $sizeX; $j++) {
      echo '<td class="codenames-table-cell"><input class="codenames-table-cell-input" type="text" name="cell-'.$j.'-'.$i.'"></td>';
    }
    echo "</tr>";
  }
?>
</table>
<br>
<br>
<input type="submit" name="new-game" value="Start Game">

</form>
</div>
</div>
<?php
  require($_SERVER['DOCUMENT_ROOT'].'/data/footer-page.php');
?>
