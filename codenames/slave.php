<?php
  $title = "Codenames - Game";
  $nav = 2;
  require($_SERVER['DOCUMENT_ROOT']."/data/header-page.php");
  require_once($_SERVER['DOCUMENT_ROOT']."/data/config/codenames_database.php");
  $database = new CodenamesDatabase();
  $database->xss($_GET);
  $database->xss($_POST);
  if(isset($_GET['code'])){
      $code =$_GET['code'];
      if($database->game_exists($code)){
          $database->add_player($code, session_id(), $_GET['name']);
      }

?>
<link rel="stylesheet" href="/css/codenames.css">
<div>
    <div id="slave-game">
    </div>
    <script type="text/javascript">
      window.onload = function(){
        setInterval(function () {
          $("#slave-game").load("/codenames/slave-game.php?code=<?php echo $code; ?>");
        }, 1000);
      }
    </script>
</div>
<?php
}else{
  echo "<div>Please specify a game code.</div>";
}
  require($_SERVER['DOCUMENT_ROOT'].'/data/footer-page.php');
?>
