<?php
  $title = "Codenames";
  $nav = 2;
  require($_SERVER['DOCUMENT_ROOT']."/data/header-page.php");
?>
<link rel="stylesheet" href="/css/codenames.css">
<div class="">

  <div class="codenames-form">
    <h3>Host a game</h3>
    <form action="host.php" method="get">
      Size: <input type="number" name="sizeX" value="5"> x <input type="number" name="sizeY" value="5"><br>
      <input type="submit" value="Create Game">
    </form>
  </div>

  <div class="codenames-form">
    <h3>Join a game</h3>
    <form action="slave.php" method="get">
      Game Code: <input type="text" name="code"><br>
      Name: <input type="text" name="name"><br>
      <input type="submit" value="Join Game">
    </form>
  </div>


</div>
<?php
  require($_SERVER['DOCUMENT_ROOT'].'/data/footer-page.php');
?>
