<?php
session_start();
require_once($_SERVER['DOCUMENT_ROOT']."/data/config/codenames_database.php");
$database = new CodenamesDatabase();
$database->xss($_POST);
$database->xss($_GET);
$code = $_GET['code'];
if($database->game_exists($code)){
echo "<h4>Game Code: $code </h4><p>";
if ($database->current_turn($code)=='r') {
  echo "Red's turn";
}
else{
  echo "Blue's turn";
}
echo "</p><p>There are ";
echo $database->selected_cells_count($code);
echo " cells selected.</p>";
?>
<br>
<table class="game-table">
<?php
  $st = $database->get_cells($code);
  $size = $database->get_size($code);
  $k = 0;
  for ($i=0; $i < $size['sizeY']; $i++) {
    echo "<tr>";
    for ($j=0; $j < $size['sizeX']; $j++) {
      $a = $st->fetch();
      echo '<td class="codenames-table-cell';
      if($a['selected']==1 && $database->is_explainer(session_id())==1) echo " cell-selected";
      if($a['revealed']==1 || $database->is_explainer(session_id())==1){
        echo ' cell-team-'.$a['team'];
      }
      echo '" onclick="'."$.get('/codenames/select-cell.php?code=$code&x=$j&y=$i')".'">'.$a["word"].'</td>';
      $k++;
    }
    echo "</tr>";
  }
?>
</table>
<?php
if($database->player_team_has_explainer($code, session_id())==0){
  ?><input class="explainer-button" type="button" onclick="$.get('/codenames/explainer-role.php?code=<?php echo $code; ?>')" value="Claim Explainer Role"><?php
}
if($database->is_explainer(session_id())){
  if($database->get_team(session_id())=='r')echo "You are the explainer of team Red.";
  else echo "You are the explainer of team Blue.";
  echo " Select up to three cells to explain to the other players.";
}
else{
  if($database->get_team(session_id())=='r')echo "You are in team Red.";
  else echo "You are in team Blue.";
}
}

?>
