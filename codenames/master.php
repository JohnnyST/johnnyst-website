<?php
  $title = "Codenames - Host Game";
  $nav = 2;
  require($_SERVER['DOCUMENT_ROOT']."/data/header-page.php");
?>
<link rel="stylesheet" href="/css/codenames.css">
<div class="">
<?php
if(isset($_GET['sizeX']))$sizeX=$_GET['sizeX'];
else $sizeX = 5;
if(isset($_GET['sizeY']))$sizeY=$_GET['sizeY'];
else $sizeY = 5;

require_once($_SERVER['DOCUMENT_ROOT']."/data/config/codenames_database.php");
if(isset($_GET['code'])){
  $database = new CodenamesDatabase();
  $database->xss($_POST);
  $database->xss($_GET);
  $code = $_GET['code'];
  if($database->game_exists($code)){
    echo "<div>The game code already exists.</div>";
  }else{
  if(isset($_POST['new-game'])){
    $database->create_game($sizeX,$sizeY,$code,$_POST);
  }
?>
<div>
    <div id="master-game">
    </div>
    <script type="text/javascript">
      window.onload = function(){
        setInterval(function () {
          $("#master-game").load("/codenames/master-game.php?code=<?php echo $code; ?>");
        }, 1000);
      }
    </script>
</div>
<?php
}}else{
  echo "<div>Please specify a game code.</div>";
}
  require($_SERVER['DOCUMENT_ROOT'].'/data/footer-page.php');
?>
