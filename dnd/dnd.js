function loadFile(filePath) {
  var result = null;
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("GET", filePath, false);
  xmlhttp.send();
  if (xmlhttp.status == 200) {
    result = xmlhttp.responseText;
  }
  return result;
}
function saveFile(fileName, fileText){
  var data = new FormData();
data.append("data" , fileText);
var xhr = (window.XMLHttpRequest) ? new XMLHttpRequest() : new activeXObject("Microsoft.XMLHTTP");
xhr.open( 'post', 'filesave.php?name='+fileName, true );
xhr.send(data);
}

var data = JSON.parse(loadFile("campaign1.json"));
var selectedPlayer = null;


function load() {
  document.getElementById('player-list').innerHTML = "";
  for (var i =0;i<data["players"].length;i++) {
      var a = document.getElementById('player-list').innerHTML += '<div class="player-select" onclick="selectPlayer(' + i + ');">' + data["players"][i]["name"] + "</div>";

  }
}
/*
saves the data object on the server
*/
function save(){
  updateData();

}
/*
Saves all form inputs to the data object
*/
function updateData(){
  savePlayer();
}
function addPlayer() {
  var i = data["players"].length;
  data["players"].push();
  load();
  selectPlayer(i);
}
/*
saves the player inputs to the data object
*/
function savePlayer() {
  if(selectedPlayer ==null)return;
  for(var i of document.querySelectorAll('#player-sheet input')){
    data["players"][selectedPlayer][i.id] = i.value;
  }
}

function selectPlayer(playerId) {
  savePlayer();
  selectedPlayer = playerId;
  loadPlayer(data["players"][playerId]);
}

function loadPlayer(player) {
  for (var key in player) {
    if (player.hasOwnProperty(key)) {
      console.log(key);
      document.getElementById(key).value = player[key];
    }
  }
}
load();
